clear all
close all
clc 

%% Parametres

%longueurs des corps
l1=0.7;
l2=0.4;
l3=0.3;
l4=0.4;
l5=0.3;
le=0.3;

%masses des corps
m1=20;
m2=6;
m3=4;
m4=6;
m5=4;
m=m1+m2+m3+m4+m5;

%distance entre rep�re du corps i et son centre de masse
cm1=0.6;
cm2=0.2;
cm3=0.15;
cm4=0.2;
cm5=0.15;

%Gain proportionnel
k1=1;
k2=k1;
k3=k1;
k4=k1;
k5=k1;

%principalement pour avoir une coupure faible
tau1=0.05;
tau2=tau1;
tau3=tau1;
tau4=tau1;
tau5=tau1;

theta=[0;0;pi/2;0;-pi/2];

%% MGD init

%simplification
c1=cos(theta(1));
s1=sin(theta(1));
c12=cos(theta(1) + theta(2));
s12=sin(theta(1) + theta(2));
c123=cos(theta(1) + theta(2) + theta(3));
s123=sin(theta(1) + theta(2) + theta(3));
c14=cos(theta(1) + theta(4));
s14=sin(theta(1) + theta(4));
c145=cos(theta(1) + theta(4) + theta(5));
s145=sin(theta(1) + theta(4) + theta(5));

%calcul des centre de masses des corps ramener au rep�re monde {x,y}
OC1=[-cm1*s1 ; cm1*c1];
OC2=[-l1*s1+le/2*c1+cm2*c12 ; l1*c1+le/2*s1-cm2*s12];
OC3=[-l1*s1+le/2*c1+l2*c12 + cm3*c123 ; l1*c1+le/2*s1+l2*s12+cm3*s123];
OC4=[-l1*s1-le/2*c1-cm4*c14 ; l1*c1-le/2*s1+cm4*s14];
OC5=[-l1*s1-le/2*c1-l4*c14-cm5*c145 ; l1*c1-le/2*s1+l4*s14-cm5*s145];

%calcul du centre de masses du robot ramener au rep�re monde {x,y}.
CdM=(1/m)*(m1*OC1 + m2*OC2 + m3*OC3 + m4*OC4 + m5*OC5);

%Pose de la main droite et gauche en fonction des variables articulaires
%(rep�re monde {x,y}).
poseR=[-l1*s1-le/2*c1-l4*c14-l5*c145; l1*c1-le/2*s1+l4*s14 - l5*s145];
poseL=[-l1*s1+le/2*c1+l2*c12+l3*c123; l1*c1+le/2*s1+l2*s12 + l3*s123];

%% Initialisation pour Simulink

XR_init=poseR(1);
XL_init=poseL(1);
CdM_init=CdM(1);

XR_desire=-0.4;
XL_desire=0.5;
XCdM_desire=0;

%% Mod�le cin�matique (Jacobienne)

%les d�riv�es des centres de masses des corps i
dOC1=[-cm1*c1 0 0 0 0];
dOC2=[-l1*c1-le/2*s1-cm2*s12 -cm2*s12 0 0 0];
dOC3=[-l1*c1-le/2*s1-l2*s12-cm3*s123 -l2*s12-cm3*s123 -cm3*s123 0 0];
dOC4=[-l1*c1+le/2*s1+cm4*s14 0 0 cm4*s14 0];
dOC5=[-l1*c1+le/2*s1+l4*s14+cm5*s145 0 0 l4*s14+cm5*s145 cm5*s145];

%les Jacobiennes du Cdm de la poseR et poseL (axe x uniquement)
JCdMx=(1/m)*(m1*dOC1+m2*dOC2+m3*dOC3+m4*dOC4+m5*dOC5);
%JR=[-l1*c1+le/2*s1+l4*s14+l5*s145 0 0 l4*s14+l5*s145 l5*s145 ;...
 %   -l1*s1-le/2*c1-l4*c14-l5*c145 0 0 -l4*c14-l5*c145 -l5*c145];
JRx=[-l1*c1+le/2*s1+l4*s14+l5*s145 0 0 l4*s14+l5*s145 l5*s145 ; 0 0 0 0 0];

%JL=[-l1*c1-le/2*s1-l2*s12-l3*s123 -l2*s12-l3*s123 -l3*s123 0 0;...
%    -l1*s1+le/2*c1+l2*c12+l3*c123 l2*c12+l3*c123 l3*c123 0 0];
JLx=[-l1*c1-le/2*s1-l2*s12-l3*s123 -l2*s12-l3*s123 -l3*s123 0 0; 0 0 0 0 0];


%% Plot

%Recalcul du MGD pour le plot
mem_coordR=[];
mem_coordL=[];
mem_coordCdM=[];
mem_torse=[];
mem_epauleR=[];
mem_epauleL=[];
mem_coudeR=[];
mem_coudeL=[];
for i=1:size(theta1)
    %Calcul du MGD
    c1=cos(theta1(i));
s1=sin(theta1(i));
c12=cos(theta1(i) + theta2(i));
s12=sin(theta1(i) + theta2(i));
c123=cos(theta1(i) + theta2(i) + theta3(i));
s123=sin(theta1(i) + theta2(i) + theta3(i));
c14=cos(theta1(i) + theta4(i));
s14=sin(theta1(i) + theta4(i));
c145=cos(theta1(i) + theta4(i) + theta5(i));
s145=sin(theta1(i) + theta4(i) + theta5(i));

OC1=[-cm1*s1;cm1*c1];
OC2=[-l1*s1+le/2*c1+cm2*c12 ; l1*c1+le/2*s1+cm2*s12];
OC3=[-l1*s1+le/2*c1+l2*c12 + cm3*c123 ; l1*c1+le/2*s1+l2*s12+cm3*s123];
OC4=[-l1*s1-le/2*c1-cm4*c14 ; l1*c1-le/2*s1-cm4*s14];
OC5=[-l1*s1-le/2*c1-l4*c14-cm5*c145 ; l1*c1-le/2*s1-l4*s14-cm5*s145];

mem_coordCdM(1:2,i)=(1/m)*(m1*OC1 + m2*OC2 + m3*OC3 + m4*OC4 + m5*OC5);
mem_coordR(1:2,i)=[-l1*s1-le/2*c1-l4*c14-l5*c145; l1*c1-le/2*s1-l4*s14 - l5*s145];
mem_coordL(1:2,i)=[-l1*s1+le/2*c1+l2*c12+l3*c123; l1*c1+le/2*s1+l2*s12 + l3*s123];
mem_torse(1:2,i)=[-l1*s1; l1*c1];
mem_epauleR(1:2,i)=[-l1*s1-le/2*c1; l1*c1-le/2*s1];
mem_epauleL(1:2,i)=[-l1*s1+le/2*c1; l1*c1+le/2*s1];
mem_coudeR(1:2,i)=[-l1*s1-le/2*c1-l4*c14; l1*c1-le/2*s1-l4*s14];
mem_coudeL(1:2,i)=[-l1*s1+le/2*c1+l2*c12; l1*c1+le/2*s1+l2*s12];

end

filename = 'R_L_CdM_yes.gif';
figure(1)
hold on;
for j=1:size(theta1)

    clf;
    plot(mem_coordR(1,j),mem_coordR(2,j),'+','markersize',20,'LineWidth',2.5,'Color','red');
    hold on;
    plot(mem_coordL(1,j),mem_coordL(2,j),'+','markersize',20,'LineWidth',2.5,'Color','red');
    plot(mem_coordCdM(1,j),mem_coordCdM(2,j),'*','markersize',10,'LineWidth',2.5,'Color','black');
    
    line([0 mem_torse(1,j)],[0 mem_torse(2,j)], 'LineWidth',2,'Color','blue');
    line([mem_torse(1,j) mem_epauleR(1,j)],[mem_torse(2,j) mem_epauleR(2,j)], 'LineWidth',2,'Color','blue');
    line([mem_torse(1,j) mem_epauleL(1,j)],[mem_torse(2,j) mem_epauleL(2,j)], 'LineWidth',2,'Color','blue'); 

    line([mem_epauleL(1,j) mem_coudeL(1,j)],[mem_epauleL(2,j) mem_coudeL(2,j)], 'LineWidth',2,'Color','blue');
    line([mem_epauleR(1,j) mem_coudeR(1,j)],[mem_epauleR(2,j) mem_coudeR(2,j)], 'LineWidth',2,'Color','blue');
    line([mem_coudeL(1,j) mem_coordL(1,j)],[mem_coudeL(2,j) mem_coordL(2,j)], 'LineWidth',2,'Color','blue');
    line([mem_coudeR(1,j) mem_coordR(1,j)],[mem_coudeR(2,j) mem_coordR(2,j) ], 'LineWidth',2,'Color','blue');

    xlim([-1, 1]);
    ylim([0, 1.5]);
   
    l1=line([XR_desire XR_desire],[0 2],'Color','green','LineStyle','-.');
    l2=line([XL_desire XL_desire],[0 2],'Color','green','LineStyle','-.');
    l3=line([XCdM_desire XCdM_desire],[0 2],'Color','green','LineStyle','-.');
 
      drawnow; 
      
      % Creation du gif
       M(j) = getframe;
       im = frame2im(M(j)); 
      [imind,cm] = rgb2ind(im,256); 
      % Write to the GIF File 
      if j == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
end
  %Title and legend
   title('Configuration atteignable : priorit� � XL, puis XR, puis XCdM');
   legend([l1 l2 l3],'Position en x main droite d�sir�e','Position en x main gauche d�sir�e','Position en x CdM d�sir�e')
    
  

